﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankMultipleShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1;
    public Rigidbody m_Shell;
    public Transform m_FireTransform;
    public Transform m_FireTransform2;
    public Transform m_FireTransform3;

    public Slider m_AimSlider;
    public AudioSource m_ShootingAudio;
    public AudioClip m_ChargingClip;
    public AudioClip m_FireClip;
    public float m_MinLaunchForce = 15f;
    public float m_MaxLaunchForce = 30f;
    public float m_MaxChargeTime = 0.75f;


    private string m_FireButton;
    private float m_CurrentLaunchForce;
    private float m_CurrentLaunchForce2;
    private float m_CurrentLaunchForce3;
    private float m_ChargeSpeed;
    private bool m_Fired;

    public bool mfire;

    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        m_FireButton = "MultipleFire" + m_PlayerNumber;

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }


    private void Update()
    {
        if(mfire)
        {
            // Track the current state of the fire button and make decisions based on the current launch force.
            m_AimSlider.value = m_MinLaunchForce;

            // If the max force has been exceeded and the shell hasn't yet been launched...
            if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
            {
                // ... use the max force and launch the shell.
                m_CurrentLaunchForce = m_MaxLaunchForce;
                m_CurrentLaunchForce2 = m_MaxLaunchForce;
                m_CurrentLaunchForce3 = m_MaxLaunchForce;
                Fire();
            }
            // Otherwise, if the fire button has just started being pressed...
            else if (Input.GetButtonDown(m_FireButton))
            {
                // ... reset the fired flag and reset the launch force.
                m_Fired = false;
                m_CurrentLaunchForce = m_MinLaunchForce;
                m_CurrentLaunchForce2 = m_MinLaunchForce;
                m_CurrentLaunchForce3 = m_MinLaunchForce;

                // Change the clip to the charging clip and start it playing.
                m_ShootingAudio.clip = m_ChargingClip;
                m_ShootingAudio.Play();
            }
            // Otherwise, if the fire button is being held and the shell hasn't been launched yet...
            else if (Input.GetButton(m_FireButton) && !m_Fired)
            {
                // Increment the launch force and update the slider.
                m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;
                m_CurrentLaunchForce2 += m_ChargeSpeed * Time.deltaTime;
                m_CurrentLaunchForce3 += m_ChargeSpeed * Time.deltaTime;

                m_AimSlider.value = m_CurrentLaunchForce;
            }
            // Otherwise, if the fire button is released and the shell hasn't been launched yet...
            else if (Input.GetButtonUp(m_FireButton) && !m_Fired)
            {
                // ... launch the shell.
                Fire();
            }
            StartCoroutine(check());
            
        }
        
    }
    private IEnumerator check()
    {
        yield return new WaitForSeconds(5f);
        mfire = false;
    }

    private void Fire()
    {
        m_Fired = true;
        Rigidbody shellInstance =
            Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
        Rigidbody shellInstance2 =
            Instantiate(m_Shell, m_FireTransform2.position, m_FireTransform2.rotation) as Rigidbody;
        Rigidbody shellInstance3 =
            Instantiate(m_Shell, m_FireTransform3.position, m_FireTransform3.rotation) as Rigidbody;
        
        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward; ;
        shellInstance2.velocity = m_CurrentLaunchForce2 * m_FireTransform2.forward; ;
        shellInstance3.velocity = m_CurrentLaunchForce3 * m_FireTransform3.forward; ;
       
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        // Reset the launch force.  This is a precaution in case of missing button events.
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_CurrentLaunchForce2 = m_MinLaunchForce;
        m_CurrentLaunchForce3 = m_MinLaunchForce;
    }
}
