﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnSpeedUpItem : MonoBehaviour
{
    public LayerMask m_TankMask;
    public float m_MaxLifeTime = 60f;
    public float m_TurnSpeed = 20f;

    public float m_ExplosionRadius = 5f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRogidbody = colliders[i].GetComponent<Rigidbody>();
            if (!targetRogidbody)
                continue;

            TankMovement targetMovement = targetRogidbody.GetComponent<TankMovement>();

            if (!targetMovement)
                continue;

            targetMovement.TurnUpMovement(m_TurnSpeed);
            Destroy(gameObject);

        }
    }
}
