﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecovaryItem : MonoBehaviour
{
    public LayerMask m_TankMask;
    public float m_MaxLifeTime = 60f;
    public float m_Recovary = 20f;

    public float m_ExplosionRadius = 5f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRogidbody = colliders[i].GetComponent<Rigidbody>();
            if (!targetRogidbody)
                continue;

            TankHealth targetHealth = targetRogidbody.GetComponent<TankHealth>();

            if (!targetHealth)
                continue;

            targetHealth.TakeRecovery(m_Recovary);
            Destroy(gameObject);

        }
    }
}
