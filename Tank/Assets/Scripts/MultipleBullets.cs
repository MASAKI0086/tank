﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleBullets : MonoBehaviour
{
    public LayerMask m_TankMask;
    public float m_MaxLifeTime = 60f;
    public float m_Speed = 20f;

    public float m_ExplosionRadius = 5f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, m_MaxLifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRogidbody = colliders[i].GetComponent<Rigidbody>();
            if (!targetRogidbody)
                continue;

            TankMultipleShooting targetMovement = targetRogidbody.GetComponent<TankMultipleShooting>();

            if (!targetMovement)
                continue;

            targetMovement.mfire=true;
            Destroy(gameObject);


        }
    }
}
